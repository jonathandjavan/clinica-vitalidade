$(function(){
    /* FORMS */
    $('.form-validate').validationEngine({
		promptPosition: 'topLeft',
		binded: false
	});
    
	$('.enviar').removeAttr('disabled'); 
	
	$('.form-ajax').ajaxForm({
		dataType: 'json',
		beforeSubmit: function(arr, $form, options){
			$('.enviar').attr('disabled','disabled');
			startLoading();
		},
		success: function(json, status, xhr, form) { 
			var id_form = form[0].id;
			
			if(json.erro != ""){
				alertify.alert(json.erro);
			}else{
				alertify.alert(json.sucesso);
				$('#'+id_form)[0].reset();
			}
		},
		error: function() { 
			alertify.alert("Erro ao processar formulário.<br>Tente novamente mais tarde.");
		},
		complete: function(){
			$('.enviar').removeAttr('disabled');
			stopLoading();
		}
	});
	
	$(".year").mask("9999");
	$(".data").mask("99/99/9999");
	$(".hora").mask("99:99");
	$(".cep").mask("99999-999");
	$(".cpf").mask("999.999.999-99");
	$(".placa_carro").mask("aaa-9999");
	$('.phone_number')  
	.mask("(99)9999-9999?9", { placeholder: "" })
	.blur(function (event) {  
		var target, phone, element;  
		target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
		phone = target.value.replace(/\D/g, '');  
		element = $(target);  
		element.unmask();  
		if(phone.length > 10) {  
			element.mask("(99)99999-999?9", { placeholder: "" });  
		} else {  
			element.mask("(99)9999-9999?9", { placeholder: "" });  
		}  
	});
	
	$('.money').maskMoney({
		prefix: 'R$ ',
		thousands: '.',
		decimal: ',',
		precision: 2,
		allowZero: true,
		affixesStay: false
	});
	
	$('.km').maskMoney({
		suffix: ' km',
		thousands: '.',
		decimal: '',
		precision: 0,
		allowZero: true,
		affixesStay: false
	});
	
	var altura_pagina = $(document).height();
	$('.loading-wrapper').css('height', altura_pagina + 'px');
	
	$('#tipo_consulta-consulta').on('change', function(){
		getPlanos('#tipo_consulta-consulta');
	});
	
	getPlanos('#tipo_consulta-consulta');
	
	$.each($('form'), function(index, val){
		$('#' + $(val).attr('id'))[0].reset();
	});
	
});

function startLoading(){
	$('.loading-wrapper').stop(true, true).fadeIn();
}

function stopLoading(){
	$('.loading-wrapper').stop(true, true).fadeOut('fast');
}

function getPlanos(id_tipo){
	
	var tipo = $(id_tipo).val();
	if(tipo != "" && tipo != 0){
		$.ajax({
			method: 'POST',
			url: 'receivers/retorna_planos.php',
			dataType: 'json',
			data: { tipo_consulta: tipo },
			beforeSend: function(){
				startLoading();
				$('#plano-consulta').html('');
			},
			success: function(json){
				if(json.erro != ""){
					//alertify.alert(json.erro);
				}else if(json.planos.length > 0){
					var html_planos = '<option value="">Convênio</option>';
					$.each(json.planos, function(index, plano){
						html_planos += '<option value="'+plano.nome+'">'+plano.nome+'</option>';
					});
					$('#plano-consulta').html(html_planos);
				}else{
					alertify.alert("Nenhum plano encontrado para a Especialidade selecionada.");
				}
			},
			error: function() { 
				alertify.alert("Erro ao consultar planos.<br>Tente novamente mais tarde.");
			},
			complete: function(){
				stopLoading();
			}
		});
	}
}