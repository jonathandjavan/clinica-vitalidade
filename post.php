<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br"> <!--<![endif]-->
<html lang="pt-br">
<head>
    <?php require_once("inc/_tags.php"); ?>
</head>
<body>

<?php require_once("inc/_header.php"); ?>

<div class="outdoor">
    <div class="center">
        <h1 class="out">Blog Vitalidade</h1>
    </div>
</div>
<div class="full sobrepor-top">
    <div class="center">
        <span class="detalhe-slider">Detalhe do Slider</span><!-- Fim do Span de detalhe-->
    </div><!-- Fim center -->
</div><!-- Fim Full -->
<div class="full sobrepor-contato">
    <div class="center">
        <span class="detalhe-faleconosco">Detalhe do Fale Conosco</span><!-- Fim do Span de detalhe-->
            <div class="bar-left">
                <ul class="blog">
                    <li>
                        <a href="javascript:;">
                            <span class="tag">Geriatria</span>
                            <span class="date">10 de Janeiro de 2016</span>
                        </a>
                    </li>
                </ul>
                    <h1 class="title-news">Dicas de ouro para se manter sempre saudável durante a velhice</h1> <!-- Titulo da Notícia -->
                        <div class="post">
                            <div class="pic" style="background: url(img/pic.png) center center no-repeat">Imagem Principal do Post</div>
                                <p class="right">Especialistas dizem que a melhor forma de continuar saudável durante a velhice é evitar o excesso de peso, fazer uma dieta saudável e praticar exercícios físicos regularmente, além de visitar sempre o médico geriatra para exames preventivos. Os cientistas, da Universidade de Linkoping, na Suécia, acreditam que os fatores de estilo de vida são vitais para manter a saúde cognitiva, psicológica e física à medida que a população envelhece. Eles apontam que até 2050 haverá 3,2 milhões de pessoas em todo o mundo envelhecido, 18 vezes mais do que em 2000.</p><!-- Fim de texto ao lado direito da imagem -->
                                    <p>O excesso de peso e a obesidade são uma ameaça, porque a gordura do estômago impacta diretamente o metabolismo do corpo e aumenta o risco de doenças como diabetes, hipertensão, câncer e problemas cardiovasculares. As pessoas também precisam se manter em movimento para manter a saúde. Atividade física regular é fundamental para ficar apto e bem em qualquer idade, não só durante a velhice.</p>
                                    <p>O estudo da Universidade de Linkoping está publicado na revista Melhor Prática e Investigação Clínica de Obstetrícia e Ginecologia.</p>
                                    <p>O excesso de peso e a obesidade são uma ameaça, porque a gordura do estômago impacta diretamente o metabolismo do corpo e aumenta o risco de doenças como diabetes, hipertensão, câncer e problemas cardiovasculares. As pessoas também precisam se manter em movimento para manter a saúde. Atividade física regular é fundamental para ficar apto e bem em qualquer idade, não só durante a velhice.</p>
                        </div><!-- Fim Box Da Postagem -->
                            <div class="share">
                                <span class='st_facebook_hcount' displayText='Facebook'></span>
                                <span class='st_twitter_hcount' displayText='Tweet'></span>
                                <span class='st_linkedin_hcount' displayText='LinkedIn'></span>
                                <span class='st_googleplus_hcount' displayText='Google +'></span>
                            </div><!-- Fim Botões de Share -->
                    <a href="javascript:;" class="morepost"><strong>+</strong> Ver todas as matérias</a><!-- Fim Botão de Carregar Mais -->
            </div><!-- Fim Sidebar Left -->
                <div class="bar-right">
                    <form action="#" class="busca">
                        <input type="text" placeholder="Buscar por...">
                    </form><!-- Fim Form -->
                    <span class="detalhe-blog">Detalhe de Divisão do Blog e os Serviços</span>
                        <h2 class="tags">Categorias</h2>
                            <div class="loadcategoria">
                                <a href="javascript:;" class="categoria">Nutrologia</a>
                                <a href="javascript:;" class="categoria">Idoso</a>
                                <a href="javascript:;" class="categoria">Dança</a>
                                <a href="javascript:;" class="categoria">Esporte</a>
                                <a href="javascript:;" class="categoria">Psicologia</a>
                                <a href="javascript:;" class="categoria">Nutrologia</a>
                                <a href="javascript:;" class="categoria">Geriatria</a>
                                <a href="javascript:;" class="categoria">Saúde</a>
                                <a href="javascript:;" class="categoria">Gourmet</a>
                                <a href="javascript:;" class="categoria">Dança</a>
                                <a href="javascript:;" class="categoria">Psicologia</a>
                                <a href="javascript:;" class="categoria">Idoso</a>
                                <a href="javascript:;" class="categoria">Nutrologia</a>
                                <a href="javascript:;" class="categoria">Dança</a>
                                <a href="javascript:;" class="categoria">Nutrologia</a>
                                <a href="javascript:;" class="categoria">Gourmet</a>
                                <a href="javascript:;" class="categoria">Idoso</a>
                                <a href="javascript:;" class="categoria">Dança</a>
                                <a href="javascript:;" class="categoria">Esporte</a>
                                <a href="javascript:;" class="categoria">Psicologia</a>
                                <a href="javascript:;" class="categoria">Nutrologia</a>
                                <a href="javascript:;" class="categoria">Geriatria</a>
                                <a href="javascript:;" class="categoria">Dança</a>
                                <a href="javascript:;" class="categoria">Esporte</a>
                            </div> <!-- Fim da Box de Carregar Categorias -->
                                <span class="detalhe-blog">Detalhe de Divisão do Blog e os Serviços</span>
                </div><!-- Fim Sidebar Right -->

                                <span class="detalhe-blog" style="float: left;">Detalhe de Divisão do Blog e os Serviços</span>
    </div><!-- Fim Center -->
</div><!-- Fim Full -->

<?php require_once("inc/_footer.php"); ?>

<script type="text/javascript">stLight.options({publisher: "d54d900f-4fbc-48c6-be74-bcf8c8b0b8f4", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
    
</body>
</html>