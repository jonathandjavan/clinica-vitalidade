<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br"> <!--<![endif]-->
<html lang="pt-br">
<head>
    <?php require_once("inc/_tags.php"); ?>
</head>
<body>

<?php require_once("inc/_header.php"); ?>

<div class="outdoor">
    <div class="center">
        <h1 class="out">Blog Vitalidade</h1>
    </div>
</div>
<div class="full sobrepor-top">
    <div class="center">
        <span class="detalhe-slider">Detalhe do Slider</span><!-- Fim do Span de detalhe-->
    </div><!-- Fim center -->
</div><!-- Fim Full -->
<div class="full sobrepor-contato">
    <div class="center">
        <span class="detalhe-faleconosco">Detalhe do Fale Conosco</span><!-- Fim do Span de detalhe-->
            <div class="bar-left">
                <ul class="allpost">
                <li>
                    <a href="javascript:;">
                        <div class="thumb" style="background: url(img/blog/01.jpg) center center no-repeat;"></div>
                        <div class="title-post">
                            <h1>Dicas de ouro para se manter saudável durante a velhice</h1>
                        </div>
                        <div class="descritivo">
                            <p>Especialistas dizem que a melhor forma de continuar saudável durante a velhice é evitar o excesso de peso, fazer <strong> &rarr;</strong></p>
                        </div>
                        <div class="clearfix"></div>
                        <span class="tag">Geriatria</span>
                        <span class="date">10 de Janeiro de 2016</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;">
                        <div class="thumb" style="background: url(img/blog/01.jpg) center center no-repeat;"></div>
                        <div class="title-post">
                            <h1>Dicas de ouro para se manter saudável durante a velhice</h1>
                        </div>
                        <div class="descritivo">
                            <p>Especialistas dizem que a melhor forma de continuar saudável durante a velhice é evitar o excesso de peso, fazer <strong> &rarr;</strong></p>
                        </div>
                        <div class="clearfix"></div>
                        <span class="tag">Geriatria</span>
                        <span class="date">10 de Janeiro de 2016</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;">
                        <div class="thumb" style="background: url(img/blog/01.jpg) center center no-repeat;"></div>
                        <div class="title-post">
                            <h1>Dicas de ouro para se manter saudável durante a velhice</h1>
                        </div>
                        <div class="descritivo">
                            <p>Especialistas dizem que a melhor forma de continuar saudável durante a velhice é evitar o excesso de peso, fazer <strong> &rarr;</strong></p>
                        </div>
                        <div class="clearfix"></div>
                        <span class="tag">Geriatria</span>
                        <span class="date">10 de Janeiro de 2016</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;">
                        <div class="thumb" style="background: url(img/blog/01.jpg) center center no-repeat;"></div>
                        <div class="title-post">
                            <h1>Dicas de ouro para se manter saudável durante a velhice</h1>
                        </div>
                        <div class="descritivo">
                            <p>Especialistas dizem que a melhor forma de continuar saudável durante a velhice é evitar o excesso de peso, fazer <strong> &rarr;</strong></p>
                        </div>
                        <div class="clearfix"></div>
                        <span class="tag">Geriatria</span>
                        <span class="date">10 de Janeiro de 2016</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;">
                        <div class="thumb" style="background: url(img/blog/01.jpg) center center no-repeat;"></div>
                        <div class="title-post">
                            <h1>Dicas de ouro para se manter saudável durante a velhice</h1>
                        </div>
                        <div class="descritivo">
                            <p>Especialistas dizem que a melhor forma de continuar saudável durante a velhice é evitar o excesso de peso, fazer <strong> &rarr;</strong></p>
                        </div>
                        <div class="clearfix"></div>
                        <span class="tag">Geriatria</span>
                        <span class="date">10 de Janeiro de 2016</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;">
                        <div class="thumb" style="background: url(img/blog/01.jpg) center center no-repeat;"></div>
                        <div class="title-post">
                            <h1>Dicas de ouro para se manter saudável durante a velhice</h1>
                        </div>
                        <div class="descritivo">
                            <p>Especialistas dizem que a melhor forma de continuar saudável durante a velhice é evitar o excesso de peso, fazer <strong> &rarr;</strong></p>
                        </div>
                        <div class="clearfix"></div>
                        <span class="tag">Geriatria</span>
                        <span class="date">10 de Janeiro de 2016</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;">
                        <div class="thumb" style="background: url(img/blog/01.jpg) center center no-repeat;"></div>
                        <div class="title-post">
                            <h1>Dicas de ouro para se manter saudável durante a velhice</h1>
                        </div>
                        <div class="descritivo">
                            <p>Especialistas dizem que a melhor forma de continuar saudável durante a velhice é evitar o excesso de peso, fazer <strong> &rarr;</strong></p>
                        </div>
                        <div class="clearfix"></div>
                        <span class="tag">Geriatria</span>
                        <span class="date">10 de Janeiro de 2016</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;">
                        <div class="thumb" style="background: url(img/blog/01.jpg) center center no-repeat;"></div>
                        <div class="title-post">
                            <h1>Dicas de ouro para se manter saudável durante a velhice</h1>
                        </div>
                        <div class="descritivo">
                            <p>Especialistas dizem que a melhor forma de continuar saudável durante a velhice é evitar o excesso de peso, fazer <strong> &rarr;</strong></p>
                        </div>
                        <div class="clearfix"></div>
                        <span class="tag">Geriatria</span>
                        <span class="date">10 de Janeiro de 2016</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;">
                        <div class="thumb" style="background: url(img/blog/01.jpg) center center no-repeat;"></div>
                        <div class="title-post">
                            <h1>Dicas de ouro para se manter saudável durante a velhice</h1>
                        </div>
                        <div class="descritivo">
                            <p>Especialistas dizem que a melhor forma de continuar saudável durante a velhice é evitar o excesso de peso, fazer <strong> &rarr;</strong></p>
                        </div>
                        <div class="clearfix"></div>
                        <span class="tag">Geriatria</span>
                        <span class="date">10 de Janeiro de 2016</span>
                    </a>
                </li>
            </ul><!-- Fim Blog -->
                <a href="javascript:;" class="morepost"><strong>+</strong> Carregar mais matérias</a><!-- Fim Botão de Carregar Mais -->
            </div><!-- Fim Sidebar Left -->
                <div class="bar-right">
                    <form action="#" class="busca">
                        <input type="text" placeholder="Buscar por...">
                    </form><!-- Fim Form -->
                    <span class="detalhe-blog">Detalhe de Divisão do Blog e os Serviços</span>
                        <h2 class="tags">Categorias</h2>
                            <div class="loadcategoria">
                                <a href="javascript:;" class="categoria">Nutrologia</a>
                                <a href="javascript:;" class="categoria">Idoso</a>
                                <a href="javascript:;" class="categoria">Dança</a>
                                <a href="javascript:;" class="categoria">Esporte</a>
                                <a href="javascript:;" class="categoria">Psicologia</a>
                                <a href="javascript:;" class="categoria">Nutrologia</a>
                                <a href="javascript:;" class="categoria">Geriatria</a>
                                <a href="javascript:;" class="categoria">Saúde</a>
                                <a href="javascript:;" class="categoria">Gourmet</a>
                                <a href="javascript:;" class="categoria">Dança</a>
                                <a href="javascript:;" class="categoria">Psicologia</a>
                                <a href="javascript:;" class="categoria">Idoso</a>
                                <a href="javascript:;" class="categoria">Nutrologia</a>
                                <a href="javascript:;" class="categoria">Dança</a>
                                <a href="javascript:;" class="categoria">Nutrologia</a>
                                <a href="javascript:;" class="categoria">Gourmet</a>
                                <a href="javascript:;" class="categoria">Idoso</a>
                                <a href="javascript:;" class="categoria">Dança</a>
                                <a href="javascript:;" class="categoria">Esporte</a>
                                <a href="javascript:;" class="categoria">Psicologia</a>
                                <a href="javascript:;" class="categoria">Nutrologia</a>
                                <a href="javascript:;" class="categoria">Geriatria</a>
                                <a href="javascript:;" class="categoria">Dança</a>
                                <a href="javascript:;" class="categoria">Esporte</a>
                            </div> <!-- Fim da Box de Carregar Categorias -->
                                <span class="detalhe-blog">Detalhe de Divisão do Blog e os Serviços</span>
                </div><!-- Fim Sidebar Right -->

                                <span class="detalhe-blog" style="float: left;">Detalhe de Divisão do Blog e os Serviços</span>
    </div><!-- Fim Center -->
</div><!-- Fim Full -->

<?php require_once("inc/_footer.php"); ?>

</body>
</html>