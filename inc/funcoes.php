<?php
function isLocalhost(){
	if($_SERVER['SERVER_NAME'] == "127.0.0.1" || $_SERVER['SERVER_NAME'] == "localhost"){
		return true;
	}else{
		return false;
	}
}

function formataParametro($str, $striptags = true){
	$str = trim($str);
	if($striptags){ $str = strip_tags($str); }
	$str = addslashes($str);
	$str = nl2br($str);
	//$str = utf8_decode($str);
	
	return $str;
}

function AnoMesDia($data){
	$data = substr($data, 0, 10);
	if($data){
		$aData = explode("/", $data);
		if(count($aData) >= 3){
			return $aData[2] . "-" . $aData[1] . "-" . $aData[0];
		}else{
			return "";
		}
	}else return "";
}

function DiaMesAno($data){
	$data = substr($data, 0, 10);
	
	if($data){
		$aData = explode("-", $data);
		if(count($aData) >= 3){
			return $aData[2] . "/" . $aData[1] . "/" . $aData[0];
		}else{
			return "";
		}
	}else return "";
}

function UltimoDiaMes($mes, $ano){
	if(empty($mes) || empty($ano)){
		$mes = date("m");
		$ano = date("Y");
	}
	$ultimo_dia = date("t", mktime(0,0,0,$mes,'01',$ano));
	return $ultimo_dia;
}

//function to encrypt the string
function cript($str){
	for($i=0; $i<=5;$i++) {
		$str=strrev(base64_encode($str)); //apply base64 first and then reverse the string
	}
	return $str;
}
//function to decrypt the string
function descript($str){
	for($i=0; $i<=5;$i++){
		$str=base64_decode(strrev($str)); //apply base64 first and then reverse the string}
	}
	return $str;
}

function validaData($data){
	$data = explode("/", $data);
	if(count($data) != 3){
		return false;
	}else{
		
		$month = $data[1];
		$day = $data[0];
		$year = $data[2];
		
		if(!checkdate($month, $day, $year)){
			return false;
		}else{
			return true;
		}
	}
}

function validaHora($horario, $have_seconds = true){
	
	if($have_seconds){
		$size = 3;
	}else{
		$size = 2;
	}
	
	$horario = explode(":", $horario);
	if(count($horario) != $size){
		return false;
	}else{
		
		$hora = $horario[0];
		if($hora < 0 || $hora > 23){
			return false;
		}else{
			$minuto = $horario[1];
			if($minuto < 0 || $minuto > 59){
				return false;
			}else{
				if($have_seconds){
					$segunto = $horario[2];
					if($segunto < 0 || $segunto > 59){
						return false;
					}else{
						return true;
					}
				}else{
					return true;
				}
			}
		}
	}
}

function removeAcentos($string, $slug = false){
	
	$string = strtolower(utf8_decode($string));
	
	// Código ASCII das vogais
	$ascii['a'] = range(224, 230);
	$ascii['e'] = range(232, 235);
	$ascii['i'] = range(236, 239);
	$ascii['o'] = array_merge(range(242, 246), array(240, 248));
	$ascii['u'] = range(249, 252);
	
	// Código ASCII dos outros caracteres
	$ascii['b'] = array(223);
	$ascii['c'] = array(231);
	$ascii['d'] = array(208);
	$ascii['n'] = array(241);
	$ascii['y'] = array(253, 255);
	foreach($ascii as $key=>$item){
		$acentos = '';
		foreach ($item AS $codigo) $acentos .= chr($codigo);
		$troca[$key] = '/['.$acentos.']/i';
	}
	$string = preg_replace(array_values($troca), array_keys($troca), $string);
	// Slug?
	if($slug){
		// Troca tudo que não for letra ou número por um caractere ($slug)
		$string = preg_replace('/[^a-z0-9]/i', $slug, $string);
		// Tira os caracteres ($slug) repetidos
		$string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
		$string = trim($string, $slug);
	}
	return $string;
}
?>