<?php
	require_once(dirname(__FILE__)."/conexao.php");
?>
<meta charset="UTF-8">
<title>Clínica Vitalidade</title>

<!-- CSS -->
<link rel="stylesheet" href="css/reset.css">
<link rel="stylesheet" href="css/jquery.bxslider.css">
<link rel="stylesheet" href="css/validationEngine.jquery.css">
<link rel="stylesheet" href="css/jquery.modal.css">
<link rel="stylesheet" href="css/alertify.core.css">
<link rel="stylesheet" href="css/alertify.default.css">
<link rel="stylesheet" href="css/main.css">

<!-- Media Queries -->
<link rel="stylesheet" href="css/phone.css">

<!-- Metas -->
<meta name="description" content="Pegar descrição">
<meta name="author" content="Symour Icaro / Jonathan Djavan / Daniel Augusto">
<meta name="keywords" content="6Keys">
<meta http-equiv="content-language" content="pt-br">

<!-- Favicon -->

<!-- Viewport -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Web Fonts -->
<link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,500,700,700italic,300,300italic' rel='stylesheet' type='text/css'>