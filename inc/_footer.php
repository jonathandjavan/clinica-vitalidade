<div class="center">
	<a href="javascript:;" class="facebook">facebook.com/<strong>vitalidadeclinica</strong></a>
   	<a href="javascript:;" class="instagram">instagram.com/<strong>vitalidadeclinica</strong></a>
</div><!-- Fim Center -->

<div class="full sobrepor-bottom">
    <div class="center-blue">
    
        <h5 id="logo-footer"><a href="index.php">Clínica Vitalidade</a></h5>
        
        <ul class="menu-bottom">
            <?php require("_menu.php"); ?>
        </ul><!-- Fim Menu -->
        
        <div class="box-atendimento-bottom">
            <span class="acv">Atendimento Clínica Vitalidade:</span>
            <span class="tell-bottom">Icon do Telefone</span>
            <ul class="number-bottom">
                <?php require("_telefones.php"); ?>
            </ul><!-- Fim Números da Clínica -->
            <span class="email-bottom"><a href="mailto:atendimento@clinicavitalidade.com">atendimento@clinicavitalidade.com</a></span>
        </div><!-- Fim Box de Atendimento -->
        
        <ul class="end">
            <li>Rua Felipe Cortez, 2000 - Lagoa Nova - Natal/RN - CEP: 59075-150</li><!-- Fim Elemento -->
            <li>Atendimento de segunda a sexta, das 7h às 18h</li><!-- Fim Elemento -->
        </ul><!-- Fim Endenreço -->
        
    </div><!-- Fim Center Azul -->
</div><!-- Fim FUll -->

<div id="gmaps" class="maps">
    <!--iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3969.2355314316687!2d-35.20661848570927!3d-5.822373495781081!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7b2ff8d4f14e23f%3A0xf318b4be7263c5dc!2sR.+Felipe+Cortez+-+Lagoa+Nova%2C+Natal+-+RN%2C+59075-150!5e0!3m2!1spt-BR!2sbr!4v1463444905540" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe-->
</div><!-- Fim Maps -->
<footer>
    <div class="detalhe-footer"></div>
</footer><!-- Fim Footer -->

<div class="loading-wrapper">
    <div class="loading-content">
        <div class="loading-img"><img src="img/loading-horizontal.gif" alt="Carregando"></div>
        <div class="loading-info">Aguarde, carregando...</div>
    </div>
</div>

<?php require_once(dirname(__FILE__)."/_js.php"); ?>