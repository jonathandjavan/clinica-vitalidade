<!-- JavaScript / Jquery -->
<script src="js/jquery.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/jquery.modal.js" type="text/javascript" charset="utf-8"></script>
<script src="js/highlight.pack.js" type="text/javascript" charset="utf-8"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyClK_JE6Rp3GRLgvWB5iGwiIZDIjERM5is" type="text/javascript"></script> -->
<script src="js/share_this.js"></script>
<script src="js/ajaxform.js"></script>
<script src="js/jquery.validationEngine-pt_BR.js"></script>
<script src="js/jquery.validationEngine.js"></script>
<script src="js/masked_input.js"></script>
<script src="js/jquery.maskMoney.js"></script>
<script src="js/alertify.min.js"></script>
<script src="js/main.js"></script>
<!-- <script>
	$(document).ready(function () {
        var coordinates = new google.maps.LatLng(-5.8223735, -35.2066185);

        var styleArray = [
            {
                featureType: "all",
                stylers: [
                    {saturation: -80}
                ]
            }, {
                featureType: "road.arterial",
                elementType: "geometry",
                stylers: [
                    {hue: "#00ffee"},
                    {saturation: 50}
                ]
            }, {
                featureType: "poi.business",
                elementType: "labels",
                stylers: [
                    {visibility: "off"}
                ]
            }
        ];

        var styledMap = new google.maps.StyledMapType(styleArray,
                {name: "Styled Map"});


        var mapOptions = {
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: false,
            center: coordinates,
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };



        var map = new google.maps.Map(document.getElementById("gmaps"),
                mapOptions);

        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');

        var marker = new google.maps.Marker({
            position: coordinates,
            map: map,
            icon: 'img/Pin_mapa.png'
        });
</script> -->