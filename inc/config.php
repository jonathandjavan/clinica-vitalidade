<?php
require_once(dirname(__FILE__)."/funcoes.php");

date_default_timezone_set('America/Recife');

define('DB_TYPE', 'mysql');
if(isLocalhost()){
	define('DB_HOST', 'localhost');
}else{
	define('DB_HOST', 'vita_site.mysql.dbaas.com.br');
}
define('DB_NAME', 'vita_site');
define('DB_USER', 'vita_site');
define('DB_PASS', 'vitadb2016');
define('DB_CHARSET', 'utf8');

define('SITE_URL', 'http://www.clinicavitalidade.com.br/');
?>