<header>
    <div class="center">
    
        <span class="acv">Atendimento Clínica Vitalidade:</span>
        <span class="tell">Icon do Telefone</span>
        
        <ul class="number">
           <?php require_once("_telefones.php"); ?>
        </ul><!-- Fim Números da Clínica -->
        
        <span class="email"><a href="mailto:atendimento@clinicavitalidade.com">atendimento@clinicavitalidade.com</a></span>
        
        <ul class="ms">
            <li><a href="javascript:;">Facebook</a></li>
            <li><a href="javascript:;">Instagram</a></li>
        </ul><!-- Fim MS -->
        
        <span class="follow">Siga-nos:</span>
        
    </div><!-- Fim Center -->
</header><!-- Fim Header -->

<div class="full">
    <div class="center">
    	
        <h1 id="logo"><a href="index.php">Clínica Vitalidade</a></h1><!-- Fim Logo -->
        <a href="#modal-call" rel="modal:open" class="ligamos">Ligamos para você</a>
        <a href="#modal-consulta" rel="modal:open" class="consulta">Agende sua consulta</a>

        <!-- Modais -->
        <div id="modal-call" style="display:none;">
            <h1 class="modal">Ligamos Pra Você <span class="close"><a href="#" rel="modal:close"></a></span></h1>
            <p class="text-call">Preencha o formulário abaixo e um de nossos atendentes ligará para você!</p>
            <form action="receivers/recebe_ligamos_voce.php" method="post" class="form-ajax form-validate clearfix" id="form-ligamos">
                
                <input type="text" placeholder="Nome:" name="nome" id="nome-ligamos" class="validate[required, minSize[2], maxSize[200]]" maxlength="200">
                
                <input type="text" placeholder="Telefone para contato:" name="telefone" id="telefone-ligamos" class="validate[groupRequired[contato-ligamos], minSize[13], maxSize[14]] phone_number" maxlength="14">
                
                <input type="text" placeholder="E-mail:" name="email" id="email-ligamos" class="validate[groupRequired[contato-ligamos], custom[email], maxSize[150]]" maxlength="150">
                
                <input type="submit" value="Enviar Formulário &rarr; " class="enviar">
                
            </form><!-- Fim Form -->
        </div><!-- Modal de ligação -->

        <div id="modal-consulta" style="display:none;">
            <h1 class="modal">Agende Sua Consulta <span class="close"><a href="#" rel="modal:close"></a></span></h1>
            <p class="text-cons">Preencha o formulário abaixo para realizar seu agendamento. Logo após um de nossos atendentes entrará em contato para confirmar. Todos os campos são obrigatórios.</p>
            <form action="receivers/recebe_agende_consulta.php" method="post" class="consulta form-ajax form-validate clearfix" id="form-consulta">
	            
                <select name="tipo_consulta" id="tipo_consulta-consulta">
                	<option value="">Especialidade</option>
                    <?php
                    	$sql_tipos = "SELECT id, tipo FROM tipo_consulta ORDER BY tipo";
						$qry_tipos = $conexao->prepare($sql_tipos);
						$qry_tipos->execute();
						while($rs_tipos = $qry_tipos->fetch()){
							?>
								<option value="<?= $rs_tipos['id']; ?>"><?= utf8_decode($rs_tipos['tipo']); ?></option>
							<?php
						}
					?>
                </select>
	            
                <select name="plano" id="plano-consulta">
                	<option value="">Convênio</option>
                </select>
                
	            <input type="text" placeholder="Data desejada:" name="data" id="data-consulta" class="validate[minSize[10], maxSize[10]] data" maxlength="10">
	            
                <input type="text" placeholder="Horário desejado:" name="hora" id="hora-consulta" class="validate[minSize[5], maxSize[5]] hora" maxlength="5">
	            
                <input type="text" placeholder="Seu nome:" name="nome" id="nome-consulta" class="validate[required, minSize[2], maxSize[200]]" maxlength="200">
	            
                <input type="text" placeholder="Seu e-mail:" name="email" id="email-consulta" class="validate[groupRequired[contato-consulta], custom[email], maxSize[150]]" maxlength="150">
	            
                <input type="text" placeholder="Telefone para contato:" name="telefone" id="telefone-consulta" class="validate[groupRequired[contato-consulta], minSize[13], maxSize[14]] phone_number" maxlength="14">
	            
                <input type="submit" value="Enviar Formulário &rarr; " class="enviar">
                
			</form><!-- Fim Form -->
        </div><!-- Modal de ligação -->

        <ul class="menu">
            <?php require_once("_menu.php"); ?>
        </ul><!-- Fim Menu -->
    </div><!-- Fim Center -->
</div><!-- Fim Full -->