<?php
require_once(dirname(__FILE__)."/config.php");
try {
	$conexao = new PDO(DB_TYPE.':host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS,
		array(
			PDO::ATTR_PERSISTENT => true,
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES ".DB_CHARSET
		)
	);
} catch (PDOException $e) {
	die("Erro ao conectar ao banco de dados: ".utf8_encode($e->getMessage()));
}
?>