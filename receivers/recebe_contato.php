<?php
require_once(dirname(__FILE__)."/../inc/conexao.php");

$retorno = array();
$retorno['erro'] = "";
$retorno['sucesso'] = "Mensagem enviada com sucesso.<br />Aguarde que em breve um de nossos consultores entrará em contato com você.";

if($_POST){
	
	$nome = isset($_POST['nome']) && $_POST['nome'] != "" ? formataParametro($_POST['nome']) : "";
	$email = isset($_POST['email']) && $_POST['email'] != "" ? formataParametro($_POST['email']) : "";
	$telefone = isset($_POST['telefone']) && $_POST['telefone'] != "" ? formataParametro($_POST['telefone']) : "";
	$celular = isset($_POST['celular']) && $_POST['celular'] != "" ? formataParametro($_POST['celular']) : "";
	$cidade = isset($_POST['cidade']) && $_POST['cidade'] != "" ? formataParametro($_POST['cidade']) : "";
	$estado = isset($_POST['estado']) && $_POST['estado'] != "" ? formataParametro($_POST['estado']) : "";
	$observacoes = isset($_POST['observacoes']) && $_POST['observacoes'] != "" ? nl2br(formataParametro($_POST['observacoes'])) : "";
	
	if(empty($nome)){
		$retorno['erro'] .= "- O campo <strong>NOME</strong> não pode ser vazio.<br>";
	}elseif(strlen($nome) > 200){
		$retorno['erro'] .= "- O campo <strong>NOME</strong> só pode ter até 200 caracteres.<br>";		
	}
	
	if(empty($telefone) && empty($celular) && empty($email)){
		$retorno['erro'] .= "- Você deve informar pelo menos uma forma de contato: <strong>TELEFONE, CELULAR ou E-MAIL</strong>.<br>";
	}else{
		if(strlen($telefone) > 14){
			$retorno['erro'] .= "- O campo <strong>TELEFONE</strong> só pode ter até 14 caracteres.<br>";
		}
		
		if(strlen($celular) > 14){
			$retorno['erro'] .= "- O campo <strong>CELULAR</strong> só pode ter até 14 caracteres.<br>";
		}
		
		if(strlen($email) > 150){
			$retorno['erro'] .= "- O campo <strong>E-MAIL</strong> não pode conter mais de 150 caracteres.<br />";
		}elseif(!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL)){
			$retorno['erro'] .= "- Endereço de <strong>E-MAIL</strong> inválido.<br />";
		}
	}
	
	if(empty($cidade)){
		$retorno['erro'] .= "- O campo <strong>CIDADE</strong> não pode ser vazio.<br>";
	}elseif(strlen($cidade) > 150){
		$retorno['erro'] .= "- O campo <strong>CIDADE</strong> só pode ter até 150 caracteres.<br>";		
	}
	
	if(empty($estado)){
		$retorno['erro'] .= "- O campo <strong>ESTADO</strong> não pode ser vazio.<br>";
	}elseif(strlen($estado) > 2){
		$retorno['erro'] .= "- O campo <strong>ESTADO</strong> só pode ter até 2 caracteres.<br>";		
	}
	
	if(empty($observacoes)){
		$retorno['erro'] .= "- O campo <strong>OBSERVAÇÕES</strong> não pode ser vazio.<br>";
	}
	
	if(empty($retorno['erro'])){
		
		$ip = $_SERVER['REMOTE_ADDR'];	
		$data_banco = date("Y-m-d H:i:s");
		
		//Insere os dados no banco
		$sql = "INSERT INTO contato (nome, email, telefone, celular, cidade, estado, ip, data) values (:nome, :email, :telefone, :celular, :cidade, :estado, :ip, :data)";
		$qry = $conexao->prepare($sql);
		$qry->bindValue(":nome", $nome);
		$qry->bindValue(":email", $email);
		$qry->bindValue(":telefone", $telefone);
		$qry->bindValue(":celular", $celular);
		$qry->bindValue(":cidade", $cidade);
		$qry->bindValue(":estado", $estado);
		$qry->bindValue(":ip", $ip);
		$qry->bindValue(":data", $data_banco);
		$qry->execute();
		
		if($retorno['erro'] == ""){
			
			//Envia a mensagem ao destinatário
			$data_cadastro = htmlentities(date("d/m/Y - G:i:s"));
			
			//CABEÇALHOS
			$headers = "MIME-Version: 1.1\r\n";
			$headers .= "Content-type: text/html; charset=utf-8\r\n";
			$headers .= "Reply-To: <".$email.">\r\n";
			$headers .= "From: ".$nome." <".$email.">";
			
			$destinatario = "danielaugustoca@gmail.com";
			
			$assunto_email = "[Clínica Vitalidade] - Contato";
			
			$cor_texto = "#008AD1";
			$texto = "<strong style=\"color:$cor_texto; font-size:18px;\">Nome</strong><br> ".$nome."<br><br>";
			if(!empty($email)){
				$texto .= "<strong style=\"color:$cor_texto; font-size:18px;\">E-mail</strong><br> ".$email."<br><br>";
			}
			if(!empty($telefone)){
				$texto .= "<strong style=\"color:$cor_texto; font-size:18px;\">Telefone</strong><br> ".$telefone."<br><br>";
			}
			if(!empty($celular)){
				$texto .= "<strong style=\"color:$cor_texto; font-size:18px;\">Celular</strong><br> ".$celular."<br><br>";
			}
			$texto = "<strong style=\"color:$cor_texto; font-size:18px;\">Cidade</strong><br> ".$cidade."<br><br>";
			$texto = "<strong style=\"color:$cor_texto; font-size:18px;\">Estado</strong><br> ".$estado."<br><br>";
			$texto = "<strong style=\"color:$cor_texto; font-size:18px;\">Observações</strong><br> ".$observacoes."<br><br>";
			
			$texto .= "<br><br><strong style=\"color:$cor_texto; font-size:18px;\">Data da solicitação</strong><br> ".$data_cadastro."<br><br>";
			$texto .= "<strong style=\"color:$cor_texto; font-size:18px;\">IP</strong><br> ".$ip."<br><br>";
			
            if(!isLocalhost()){
			    $envio = mail($destinatario, $assunto_email, wordwrap($texto), $headers) or $erro .= "Erro ao enviar e-mail. Tente novamente.";
            }
			
		}
			
	}
	
}else{
	$retorno['erro'] = "Erro ao enviar dados. Tente novamente mais tarde!";
}
echo json_encode($retorno);
?>