<?php
require_once(dirname(__FILE__)."/../inc/conexao.php");

$retorno = array();
$retorno['erro'] = "";
$retorno['sucesso'] = "Mensagem enviada com sucesso.<br />Aguarde que em breve um de nossos consultores entrará em contato com você.";

if($_POST){
	
	$tipo_consulta = isset($_POST['tipo_consulta']) && $_POST['tipo_consulta'] != "" ? formataParametro($_POST['tipo_consulta']) : "";
	$plano = isset($_POST['plano']) && $_POST['plano'] != "" ? formataParametro($_POST['plano']) : "";
	$data = isset($_POST['data']) && $_POST['data'] != "" ? formataParametro($_POST['data']) : "";
	$hora = isset($_POST['hora']) && $_POST['hora'] != "" ? formataParametro($_POST['hora']) : "";
	$nome = isset($_POST['nome']) && $_POST['nome'] != "" ? formataParametro($_POST['nome']) : "";
	$email = isset($_POST['email']) && $_POST['email'] != "" ? formataParametro($_POST['email']) : "";
	$telefone = isset($_POST['telefone']) && $_POST['telefone'] != "" ? formataParametro($_POST['telefone']) : "";
	
	if(!empty($tipo_consulta)){
	    $sql_tipos = "SELECT id, tipo FROM tipo_consulta WHERE id = :tipo_consulta";
		$qry_tipos = $conexao->prepare($sql_tipos);
		$qry_tipos->bindValue(":tipo_consulta", $tipo_consulta);
		$qry_tipos->execute();
		if($qry_tipos->rowCount() > 0){
			$rs_tipos = $qry_tipos->fetch();
			$tipo_consulta = $rs_tipos['tipo'];
		}else{
			$tipo_consulta = "";
		}
	}
	
	if(empty($nome)){
		$retorno['erro'] .= "- O campo <strong>NOME</strong> não pode ser vazio.<br>";
	}elseif(strlen($nome) > 200){
		$retorno['erro'] .= "- O campo <strong>NOME</strong> só pode ter até 200 caracteres.<br>";		
	}
	
	if(empty($telefone) && empty($email)){
		$retorno['erro'] .= "- Você deve informar pelo menos uma forma de contato: <strong>TELEFONE e/ou E-MAIL</strong>.<br>";
	}else{
		if(strlen($telefone) > 14){
			$retorno['erro'] .= "- O campo <strong>TELEFONE</strong> só pode ter até 14 caracteres.<br>";
		}
		
		if(strlen($email) > 150){
			$retorno['erro'] .= "- O campo <strong>E-MAIL</strong> não pode conter mais de 150 caracteres.<br />";
		}elseif(!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL)){
			$retorno['erro'] .= "- Endereço de <strong>E-MAIL</strong> inválido.<br />";
		}
	}
	
	if(!empty($data)){
		if(!validaData($data)){
			$retorno['erro'] .= "- A  <strong>DATA</strong> é inválida.<br />";
		}else{
			if(strtotime(AnoMesDia($data)) < strtotime(date("Y-m-d"))){
				$retorno['erro'] .= "- A  <strong>DATA</strong> não pode ser menor do que a data atual (".date("d/m/Y").").<br />";
			}
		}
	}
	
	if(!empty($hora)){
		if(!validaHora($hora, false)){
			$retorno['erro'] .= "- A  <strong>HORA</strong> é inválida.<br />";
		}
	}
	
	if(empty($retorno['erro'])){
		
		$ip = $_SERVER['REMOTE_ADDR'];	
		$data_banco = date("Y-m-d H:i:s");
		
		//Insere os dados no banco
		$sql = "INSERT INTO agende_consulta (nome, email, telefone, tipo_consulta, plano, data, hora, ip, data_cadastro) values (:nome, :email, :telefone, :tipo_consulta, :plano, :data, :hora, :ip, :data_cadastro)";
        $qry = $conexao->prepare($sql);
        $qry->bindValue(":nome", $nome);
        $qry->bindValue(":email", $email);
        $qry->bindValue(":telefone", $telefone);
		$qry->bindValue(":tipo_consulta", $tipo_consulta);
		$qry->bindValue(":plano", $plano);
		$qry->bindValue(":data", AnoMesDia($data));
		$qry->bindValue(":hora", $hora);
        $qry->bindValue(":ip", $ip);
        $qry->bindValue(":data_cadastro", $data_banco);
        $qry->execute();
		
		if($retorno['erro'] == ""){
			
			//Envia a mensagem ao destinatário
			$data_cadastro = htmlentities(date("d/m/Y - G:i:s"));
			
			//CABEÇALHOS
			$headers = "MIME-Version: 1.1\r\n";
			$headers .= "Content-type: text/html; charset=utf-8\r\n";
			$headers .= "Reply-To: <".$email.">\r\n";
			$headers .= "From: ".$nome." <".$email.">";
			
			$destinatario = "danielaugustoca@gmail.com";
			
			$assunto_email = "[Clínica Vitalidade] - Agende sua Consulta";
			
			$cor_texto = "#008AD1";
			if(!empty($tipo_consulta)){
				$texto = "<strong style=\"color:$cor_texto; font-size:18px;\">Especialidade</strong><br> ".$tipo_consulta."<br><br>";
			}
			if(!empty($plano)){
				$texto = "<strong style=\"color:$cor_texto; font-size:18px;\">Convênio</strong><br> ".$plano."<br><br>";
			}
			$texto = "<strong style=\"color:$cor_texto; font-size:18px;\">Nome</strong><br> ".$nome."<br><br>";
			if(!empty($email)){
				$texto .= "<strong style=\"color:$cor_texto; font-size:18px;\">E-mail</strong><br> ".$email."<br><br>";
			}
			if(!empty($telefone)){
				$texto .= "<strong style=\"color:$cor_texto; font-size:18px;\">Telefone</strong><br> ".$telefone."<br><br>";
			}
			if(!empty($data)){
				$texto = "<strong style=\"color:$cor_texto; font-size:18px;\">Data desejada</strong><br> ".$data."<br><br>";
			}
			if(!empty($hora)){
				$texto = "<strong style=\"color:$cor_texto; font-size:18px;\">Horário desejado</strong><br> ".$hora."<br><br>";
			}
			$texto .= "<br><br><strong style=\"color:$cor_texto; font-size:18px;\">Data da solicitação</strong><br> ".$data_cadastro."<br><br>";
			$texto .= "<strong style=\"color:$cor_texto; font-size:18px;\">IP</strong><br> ".$ip."<br><br>";
			
            if(!isLocalhost()){
			    $envio = mail($destinatario, $assunto_email, wordwrap($texto), $headers) or $erro .= "Erro ao enviar e-mail. Tente novamente.";
            }
			
		}
			
	}
	
}else{
	$retorno['erro'] = "Erro ao enviar dados. Tente novamente mais tarde!";
}
echo json_encode($retorno);
?>