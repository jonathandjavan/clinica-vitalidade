<?php
require_once(dirname(__FILE__)."/../inc/conexao.php");

$retorno = array();
$retorno['erro'] = "";
$retorno['planos'] = array();

if($_POST){
	
	$tipo_consulta = isset($_POST['tipo_consulta']) && $_POST['tipo_consulta'] != "" ? formataParametro($_POST['tipo_consulta']) : "";
	
	if(empty($tipo_consulta)){
		$retorno['erro'] .= "- O campo <strong>ESPECIALIDAE</strong> não pode ser vazio.<br>";
	}
	
	if(empty($retorno['erro'])){
		
		$sql = "SELECT id, nome FROM plano WHERE id IN (SELECT plano_id FROM plano_tipo WHERE tipo_consulta_id = :tipo_consulta) AND id <> 8";
		$qry = $conexao->prepare($sql);
		$qry->bindValue(":tipo_consulta", $tipo_consulta);
		$qry->execute();
		
		while($rs = $qry->fetch()){
			$retorno['planos'][] = array(
				'id' => $rs['id'],
				'nome' => utf8_decode($rs['nome'])
			);
		}
			
	}
	
}else{
	$retorno['erro'] = "Erro ao enviar dados. Tente novamente mais tarde!";
}
echo json_encode($retorno);
?>