<?php
require_once(dirname(__FILE__)."/../inc/conexao.php");

$retorno = array();
$retorno['erro'] = "";
$retorno['sucesso'] = "Mensagem enviada com sucesso.<br />Aguarde que em breve um de nossos atendentes entrará em contato com você.";

if($_POST){
	
	$nome = isset($_POST['nome']) && $_POST['nome'] != "" ? formataParametro($_POST['nome']) : "";
	$telefone = isset($_POST['telefone']) && $_POST['telefone'] != "" ? formataParametro($_POST['telefone']) : "";
	$email = isset($_POST['email']) && $_POST['email'] != "" ? formataParametro($_POST['email']) : "";
	
	if(empty($nome)){
		$retorno['erro'] .= "- O campo <strong>NOME</strong> não pode ser vazio.<br>";
	}elseif(strlen($nome) > 200){
		$retorno['erro'] .= "- O campo <strong>NOME</strong> só pode ter até 200 caracteres.<br>";		
	}
	
	if(empty($telefone) && empty($email)){
		$retorno['erro'] .= "- Você deve informar pelo menos uma forma de contato: <strong>TELEFONE e/ou E-MAIL</strong>.<br>";
	}else{
		if(strlen($telefone) > 14){
			$retorno['erro'] .= "- O campo <strong>TELEFONE</strong> só pode ter até 14 caracteres.<br>";
		}
		
		if(strlen($email) > 150){
			$retorno['erro'] .= "- O campo <strong>E-MAIL</strong> não pode conter mais de 150 caracteres.<br />";
		}elseif(!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL)){
			$retorno['erro'] .= "- Endereço de <strong>E-MAIL</strong> inválido.<br />";
		}
	}
	
	if(empty($retorno['erro'])){
		
		$ip = $_SERVER['REMOTE_ADDR'];	
		$data_banco = date("Y-m-d H:i:s");
		$data_verifica = date("Y-m-d");
		$hora_verifica = date("H");
		
		//Verifica se a pessoa entrou em contato na última hora
		$sql_verifica = "SELECT * FROM ligamos_voce WHERE (email = :email OR telefone = :telefone) AND DATE(data_envio) = :data_verifica AND HOUR(data_envio) = :hora_verifica";
		
		$qry_verifica = $conexao->prepare($sql_verifica);
		$qry_verifica->bindValue(":email", $email);
        $qry_verifica->bindValue(":telefone", $telefone);
        $qry_verifica->bindValue(":data_verifica", $data_verifica);
        $qry_verifica->bindValue(":hora_verifica", $hora_verifica);
		$qry_verifica->execute();
		if($qry_verifica->rowCount() == 0){
			
			//Insere os dados no banco
			$sql = "INSERT INTO ligamos_voce (nome, email, telefone, ip, data_envio) values (:nome, :email, :telefone, :ip, :data_envio)";
	        $qry = $conexao->prepare($sql);
	        $qry->bindValue(":nome", $nome);
	        $qry->bindValue(":email", $email);
	        $qry->bindValue(":telefone", $telefone);
	        $qry->bindValue(":ip", $ip);
	        $qry->bindValue(":data_envio", $data_banco);
	        $qry->execute();
			
			if($retorno['erro'] == ""){
				
				//Envia a mensagem ao destinatário
				$data = htmlentities(date("d/m/Y - G:i:s"));
				
				//CABEÇALHOS
				$headers = "MIME-Version: 1.1\r\n";
				$headers .= "Content-type: text/html; charset=utf-8\r\n";
				$headers .= "Reply-To: <".$email.">\r\n";
				$headers .= "From: ".$nome." <".$email.">";
				
				$destinatario = "danielaugustoca@gmail.com";
				
				$assunto_email = "[Clínica Vitalidade] - Ligamos para você";
				
				$cor_texto = "#008AD1";
				$texto = "<strong style=\"color:$cor_texto; font-size:18px;\">Nome</strong><br> ".$nome."<br><br>";
				$texto .= "<strong style=\"color:$cor_texto; font-size:18px;\">E-mail</strong><br> ".$email."<br><br>";
				$texto .= "<strong style=\"color:$cor_texto; font-size:18px;\">Telefone</strong><br> ".$telefone."<br><br>";
				$texto .= "<br><br><strong style=\"color:$cor_texto; font-size:18px;\">Data</strong><br> ".$data."<br><br>";
				//$texto .= "<strong style=\"color:$cor_texto; font-size:18px;\">IP</strong><br> ".$ip."<br><br>";
				
	            if(!isLocalhost()){
				    $envio = mail($destinatario, $assunto_email, wordwrap($texto), $headers) or $erro .= "Erro ao enviar e-mail. Tente novamente.";
	            }
				
			}
			
		}else{
			$retorno['erro'] = "Você já efetuou esta solicitação.<br />Em breve um atendente irá entrar em contato com você ou espere alguns minutos para tentar novamente.";
		}
			
	}
	
}else{
	$retorno['erro'] = "Erro ao enviar dados. Tente novamente mais tarde!";
}
echo json_encode($retorno);
?>