<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br"> <!--<![endif]-->
<html lang="pt-br">
<head>
    <?php require_once("inc/_tags.php"); ?>
</head>
<body>

<?php require_once("inc/_header.php"); ?>

<div class="outdoor">
    <div class="center">
        <h1 class="out">Fale com a Clínica Vitalidade</h1>
    </div>
</div>
<div class="full sobrepor-top">
    <div class="center">
        <span class="detalhe-slider">Detalhe do Slider</span><!-- Fim do Span de detalhe-->
    </div><!-- Fim center -->
</div><!-- Fim Full -->
<div class="full sobrepor-contato">
    <div class="center">
        <span class="detalhe-faleconosco">Detalhe do Fale Conosco</span><!-- Fim do Span de detalhe-->
            <h1>A Clínica Vitalidade quer ouvir você</h1>
                <h3 class="uper">Preencha o formulário abaixo e deixe sua mensagem. Em breve estraremos em contato.</h3>
                    <form action="receivers/recebe_contato.php" method="post" class="form-ajax form-validate clearfix" id="form-contato">
                        <div class="infos-left clearfix">
                            <input type="text" placeholder="Nome:" name="nome" id="nome-contato" class="validate[required, minSize[2], maxSize[200]]" maxlength="200">
                            <input type="text" placeholder="Telefone:" name="telefone" id="telefone-contato" class="med validate[groupRequired[contato-contato], minSize[13], maxSize[14]] phone_number" maxlength="14">
                            <input type="text" placeholder="Celular:" style="margin-left: 3.44827586206897%; " name="celular" id="celular-contato" class="med validate[groupRequired[contato-contato], minSize[13], maxSize[14]] phone_number" maxlength="14">
                        </div><!-- Fim Divisão Esquerda -->
                        <div class="infos-right clearfix">
                            <input type="text" placeholder="Email:" name="email" id="email-contato" class="validate[groupRequired[contato], custom[email], maxSize[150]]" maxlength="150">
                            
                            <input type="text" placeholder="Cidade:" name="cidade" id="cidade-contato" class="med validate[required, minSize[2], maxSize[200]]" maxlength="150">
                            
                            <select style="margin-left: 3.44827586206897%; " name="estado" id="estado-contato" class="validate[required]">
                                <option value="">Estado:</option>
								<option value="AC">Acre</option>
								<option value="AL">Alagoas</option>
								<option value="AP">Amapá</option>
								<option value="AM">Amazonas</option>
								<option value="BA">Bahia</option>
								<option value="CE">Ceará</option>
								<option value="DF">Distrito Federal</option>
								<option value="ES">Espirito Santo</option>
								<option value="GO">Goiás</option>
								<option value="MA">Maranhão</option>
								<option value="MS">Mato Grosso do Sul</option>
								<option value="MT">Mato Grosso</option>
								<option value="MG">Minas Gerais</option>
								<option value="PA">Pará</option>
								<option value="PB">Paraíba</option>
								<option value="PR">Paraná</option>
								<option value="PE">Pernambuco</option>
								<option value="PI">Piauí</option>
								<option value="RJ">Rio de Janeiro</option>
								<option value="RN">Rio Grande do Norte</option>
								<option value="RS">Rio Grande do Sul</option>
								<option value="RO">Rondônia</option>
								<option value="RR">Roraima</option>
								<option value="SC">Santa Catarina</option>
								<option value="SP">São Paulo</option>
								<option value="SE">Sergipe</option>
								<option value="TO">Tocantins</option>
                            </select>
                        </div><!-- Fim Divisão Direita -->
                        <textarea name="observacoes" id="observacoes-contato" class="validate[required]" cols="30" rows="10" placeholder="Observações:"></textarea>
                            <input type="submit" value="ENVIAR MENSAGEM &rarr;" class="enviar">
                    </form><!-- Fim Form -->

                                <span class="detalhe-blog">Detalhe de Divisão do Blog e os Serviços</span>
    </div><!-- Fim Center -->
</div><!-- Fim Full -->

<?php require_once("inc/_footer.php"); ?>

</body>
</html>