<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br"> <!--<![endif]-->
<html lang="pt-br">
<head>
    <?php require_once("inc/_tags.php"); ?>

</head>
<body>

<?php require_once("inc/_header.php"); ?>

<div class="outdoor">
    <div class="center">
        <h1 class="out">Conheça a Clínica Vitalidade</h1>
    </div>
</div>
<div class="full sobrepor-top">
    <div class="center">
        <span class="detalhe-slider">Detalhe do Slider</span><!-- Fim do Span de detalhe-->
    </div><!-- Fim center -->
</div><!-- Fim Full -->
<div class="full sobrepor-contato">
    <div class="center">
        <span class="detalhe-faleconosco">Detalhe do Fale Conosco</span><!-- Fim do Span de detalhe-->
            <div class="text-left">
                <p>A <span>Clínica Vitalidade</span> foi criada em 2004, pela <strong>Drª. Andrea de Albuquerque Maia</strong> e o <strong>Dr. Jesus Rolly Dominguez Gutierrez</strong>. Hoje a clínica está localizada na Rua Felipe Cortaz, 2000 – Lagoa Nova | Natal/RN. Atendendo as especialidades Geriatria e Nutrologia para pacientes de planos de saúde e particular.</p>
                <p>Com um ambiente harmonioso, climatizado e extremamente confortável, a clínica conta com uma área exclusiva para crianças onde os pequenos podem desenhar e brincar, assim, você que é mãe não vai precisar se preocupar na hora da sua consulta.</p>
            </div><!-- Fim Do Texto do lado Esquerdo -->
            <div class="text-right">
                <p>O ambiente também dispõe de uma academia com equipamentos avançados e profissionais qualificados para orientar os alunos, além de um café onde você pode descansar e se alimentar depois do treino.</p>
                <p>Tudo isso e muito mais foi feito especialmente para você! Por isso, faça uma visita a clínica Vitalidade e confira o quanto prezamos por seu total conforto, comodidade e principalmente por sua saúde!</p>
            </div><!-- Fim Do Texto do lado Direito -->
            
            <div class="clearfix"></div>
            
            <ul class="slider">
                <li style="background: url(img/slider/sobre/01.png) center center no-repeat"></li><!-- Fim Elemento -->
                <li style="background: url(img/slider/sobre/02.png) center center no-repeat"></li><!-- Fim Elemento -->
            </ul><!-- Fim Slider de Sobre -->
    		<span class="detalhe-blog" style="float: left; margin-top: 2.54237288135593%;">Detalhe de Divisão do Blog e os Serviços</span>
    </div><!-- Fim Center -->
</div><!-- Fim Full -->

<?php require_once("inc/_footer.php"); ?>

<script>
    $(function(){
        $('.slider').bxSlider({
            pager: false,
            controls: false,
            randomStart: true,
            auto: true,
            speed: 1000,
            pause: 6000
        });
    });
</script>
</body>
</html>