<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br"> <!--<![endif]-->
<html lang="pt-br">
<head>
    <?php require_once("inc/_tags.php"); ?>
</head>
<body>

<?php require_once("inc/_header.php"); ?>

<ul class="bxslider">
    <li style="background: url(img/slider/01.png) top center no-repeat">
        <div class="center">
            
        </div><!-- Fim Center -->
     </li><!-- Fim Elemento -->
     <li style="background: url(img/slider/02.png) top center no-repeat">
        <div class="center">
            
        </div><!-- Fim Center -->
     </li><!-- Fim Elemento -->
</ul><!-- Fim Slider -->
<div class="full sobrepor-top">
    <div class="center">
        <span class="detalhe-slider">Detalhe do Slider</span><!-- Fim do Span de detalhe-->
            <ul class="servico">
                <li style="background: url(img/servico/01.jpg) center center no-repeat;">
                    <a href="javascript:;">
                        <div class="effect"></div>
                        <span class="title-servico">Clínica Vitalidade</span>
                    </a>
                </li><!-- Fim Elemento -->
                <li style="background: url(img/servico/02.jpg) center center no-repeat;">
                    <a href="javascript:;">
                        <div class="effect"></div>
                        <span class="title-servico">Treino Cognitivo</span>
                    </a>
                </li><!-- Fim Elemento -->
                <li style="background: url(img/servico/03.jpg) center center no-repeat;">
                    <a href="javascript:;">
                        <div class="effect"></div>
                        <span class="title-servico">Estética</span>
                    </a>
                </li><!-- Fim Elemento -->
                <li style="background: url(img/servico/04.jpg) center center no-repeat;">
                    <a href="javascript:;">
                        <div class="effect"></div>
                        <span class="title-servico">Dança</span>
                    </a>
                </li><!-- Fim Elemento -->
                <li style="background: url(img/servico/05.jpg) center center no-repeat;">
                    <a href="javascript:;">
                        <div class="effect"></div>
                        <span class="title-servico" style="font-size: 2.4em;">Academia e Treino Funcional</span>
                    </a>
                </li><!-- Fim Elemento -->
                <li style="background: url(img/servico/06.jpg) center center no-repeat;">
                    <a href="javascript:;">
                        <div class="effect"></div>
                        <span class="title-servico">Biblioteca e Café Gourmet</span>
                    </a>
                </li><!-- Fim Elemento -->
            </ul><!-- Fim Serviços -->
                <span class="detalhe-blog">Detalhe de Divisão do Blog e os Serviços</span>
                    <span class="line-center">Linha Central do Blog</span>
                        <h1>Blog <strong>Vitalidade</strong></h1>
                        <h3>Aprenda formas de viver com mais qualidade de vida.</h3>
                        <h3>Acompanhe nossas postagens e descubra!</h3>
            <ul class="blog">
                <li>
                    <a href="javascript:;">
                        <div class="thumb" style="background: url(img/blog/01.jpg) center center no-repeat;"></div>
                        <div class="title-post">
                            <h1>Dicas de ouro para se manter saudável durante a velhice</h1>
                        </div>
                        <div class="descritivo">
                            <p>Especialistas dizem que a melhor forma de continuar saudável durante a velhice é evitar o excesso de peso, fazer <strong> &rarr;</strong></p>
                        </div>
                        <div class="clearfix"></div>
                        <span class="tag">Geriatria</span>
                        <span class="date">10 de Janeiro de 2016</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;">
                        <div class="thumb" style="background: url(img/blog/01.jpg) center center no-repeat;"></div>
                        <div class="title-post">
                            <h1>Dicas de ouro para se manter saudável durante a velhice</h1>
                        </div>
                        <div class="descritivo">
                            <p>Especialistas dizem que a melhor forma de continuar saudável durante a velhice é evitar o excesso de peso, fazer <strong> &rarr;</strong></p>
                        </div>
                        <div class="clearfix"></div>
                        <span class="tag">Geriatria</span>
                        <span class="date">10 de Janeiro de 2016</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;">
                        <div class="thumb" style="background: url(img/blog/01.jpg) center center no-repeat;"></div>
                        <div class="title-post">
                            <h1>Dicas de ouro para se manter saudável durante a velhice</h1>
                        </div>
                        <div class="descritivo">
                            <p>Especialistas dizem que a melhor forma de continuar saudável durante a velhice é evitar o excesso de peso, fazer <strong> &rarr;</strong></p>
                        </div>
                        <div class="clearfix"></div>
                        <span class="tag">Geriatria</span>
                        <span class="date">10 de Janeiro de 2016</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;">
                        <div class="thumb" style="background: url(img/blog/01.jpg) center center no-repeat;"></div>
                        <div class="title-post">
                            <h1>Dicas de ouro para se manter saudável durante a velhice</h1>
                        </div>
                        <div class="descritivo">
                            <p>Especialistas dizem que a melhor forma de continuar saudável durante a velhice é evitar o excesso de peso, fazer <strong> &rarr;</strong></p>
                        </div>
                        <div class="clearfix"></div>
                        <span class="tag">Geriatria</span>
                        <span class="date">10 de Janeiro de 2016</span>
                    </a>
                </li>
            </ul><!-- Fim Blog -->
            <span class="detalhe-blog">Detalhe de Divisão do Blog e os Serviços</span>
            <span class="line-center">Linha Central do Blog</span>
            <h1>O que falam nossos clientes</h1>
            <ul class="depoimentos">
            	<li>
                	<p class="depoimento">Estou completamente satisfeito com a minha experiência com o Dr. jesus rolly se o nível de profissionalismo como a própria cirurgia. Ele nos informa sobre o que é, o que pode ser feito e considera o que queremos, enquanto aconselhando-nos. Eu recomendo sem hesitação. Mais uma vez obrigado Dr. jesus rolly.</p>
                    <span class="name-depoimento">Andréia Maia, cliente do Dr. Jesus Rolly - Geriatra</span>
                </li><!-- Fim Elemento -->
            </ul><!-- Fim Depoimentos -->
            <div class="center-buttons">
            	<a href="#modal-call" rel="modal:open" class="ligamos">Ligamos para você</a>
                <a href="#modal-consulta" rel="modal:open" class="consulta">Agende sua consulta</a>
            </div><!-- Fim da Box para centralizar os botões -->
            
            <span class="detalhe-blog">Detalhe de Divisão do Blog e os Serviços</span>
            
            <h4><strong>Aceitamos os planos e convênios -</strong> Consultar especialidade</h4>
            <div class="planos">Planos de Convênios</div> <!-- FFim Planos -->
            
    </div><!-- Fim Center -->
</div><!-- Fim Full -->

<?php require_once("inc/_footer.php"); ?>

<script>
	$(function(){
		$('.bxslider').bxSlider({
			pager: false,
			controls: false,
			randomStart: true,
			auto: true,
			speed: 1000,
			pause: 6000,
			mode: 'fade'
		});
		
		$( "ul.servico li .effect" ).mouseenter(function() {
			$(this).fadeOut('slow');
		});
		$( "ul.servico li" ).mouseleave(function() {
			$(".effect").fadeIn('slow');
		});
		
	});
</script>

</body>
</html>